> - Collaboration: [Ableton](https://www.ableton.com/en/)
> - Distributed by [Ableton](https://www.ableton.com/en/packs/ircamax-1/)
> - [Technical Support](https://www.ableton.com/en/help/)

-	**IM-FXSequencer** includes 4 effects in the same [Max for Live](https://www.ableton.com/en/live/max-for-live/) device: a Delay line, a Freeze, a Transposer and a Ring Modulator. A step sequencer, synchronized on the Live tempo, trigs exponential Attack and Release envelopes and defines to which effects the signal has to be send on each step.
-	**IM-MultibandDelay** uses an FFT processing to split the spectrum in 8 frequency bands and apply a different delay and feedback to each of them. It is also possible to set a different panning and volume for each bands. Automatic motions functions can be used to continually change the result of the transformation and create dynamic polyrhythm.
-	**IM-SimpleTransp** uses the SuperVP technology in a simple device to perform realistic transpositions with formants preservation. The transposition amount can be controlled by MIDI using the MIDI Send device transmitting notes from another Live instrument track. It also gives a quick access to the Remix capabilities of the SuperVP engine in order to change the Sinusoïds/Noise balance or remove the transients on a Live track.
-	**IM-Transp** includes the same functions as in SimpleTransp, but with two additional sequencer synchronized on the Live tempo to control the pitch and the transpositions of the formants.
-	**IM-Scrub** adds a position Matrix and scrub/scratch functions to the Transp device. It can record audio on the fly and can reorder sound material divided in slices. Its time manipulations capabilities associated with the Remix functions of SuperVP make Scrub the ultimate tool for sound transformation and manipulation.
-	**IM-Mover** offers a different way to control time position and transposition performed by the SuperVP engine. It displays the sound on a big waveform on which the user can move using the mouse, its MIDI controllers or a joystick. It can be easily use to improvise on the sound material thanks to its synchronized random motions capabilities.

## Instrument ##

**IM-SuperVPSynth** embeds the SuperVP technology into a sampler instrument. The SuperVP’s transposition quality enables the production of realistic instruments or voices using just one sample. Time stretch and remix functions of the engine authorize deep time and timbre manipulations on sound files. The instrument also includes a multimode filter, 2 envelopes, and an LFO to perform more complex modulations.

## Ircamax Volume 1 ”lesson” ##

- **IM-FXSequencer**

This device includes 4 effects in the same Max for Live device: a Delay line, a Freeze, a Transposer and a Ring Modulator. A step sequencer, synchronized on the Live tempo, trigs exponential Attack and  Release envelopes and defines to which effects the signal has to be send for each steps. Two sequences can be programmed, for the left and the right channel.

![FXSequencer](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/im-fxsequencerfull.png)

The left part of the device is dedicated to the sequencer. Choose a sync rate in 16th notes and define how many steps you want to use. Fill the sequence to define which effect has to be triggered on each step.
Random function are available in order to fill the sequencer automatically.

![sequencer]( https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/capture-d%E2%80%99e%CC%81cran-2014-07-02-a%CC%80-12.48.12.png)

Use the different tabs to change the effects parameters. You can define a different Attack, Release and Gain for each of them and other parameters for the Left and the Right channels.

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/transptab.png) ![Ring](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/ringmodetab.png) ![Freeze](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/freezetab.png) ![Delay](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/delaytab.png)

A global section provides two Knobs to globally change the Attacks and Release of the 4 effects as a percentage of the time programed in the tabs. This section also allows to switch each effect on and off.

![](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/globalsection.png)

-	**IM-MultibandDelay**
This device uses an FFT processing to split the spectrum in 8 frequency bands and applies a different delay and feedback to each of them. It is also possible to set a different panning and volume for each bands. Automatic motions functions can be used to continually change the result of the transformation and create dynamic polyrhythm.

![Multi Band Delay](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/multibanddelay_full.png)

The left part of the device is dedicated to the Delay Times. First, you can change the frequency range of the device by moving the Low and High band limits. Then a multi slider allows to define delay times in 16th notes from 0 to 16.

![Delay Time](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/delays-freqlimits.png)

The next sections allows to define the feedbacks and the panning for each frequency bands.

![Feedback section](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/feedback.png ) ![Pan section](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/feedback-1.png)

In those tree sections, sliders can be moved automatically by using the Sync button and choosing an action: Up, Down, Random. It is also possible to change all the programmed values using the percentage slider assigned to a MIDI controller.
The last section sets the volume and allows to switch each band on and off.

![Volume section](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/volume_section.png)

-	**IM-SimpleTransp**
IM-Transp embed the SuperVP Ircam technology to perform Real Time Transpositions and Remix manipulations on an incoming signal.

![IM-Transp](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/im-simpletransp.png)

The first part of the interface is dedicated to the transposition and formants (spectral envelope) parameters. Use the left side to define the transposition amount. This parameter can also be changed by MIDI using a IM-MidiSend device on a MIDI track. Use the right side to transpose the spectral envelope separately. In order to perform a good envelope preservation, you must set a good value of the Max Freq parameter. It represents the Maximum pitch (in frequency) produced by the signal.
This envelope preservation function is particularly efficient on voices, avoiding the « Mikey Mouse » effect but it also works well on instruments by keeping the filtering of the body unchanged when transposing. It produces a more natural transposition.

![Transp Fmts section](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/transpfrmts_section.png)

When the Remix section is activated, it allows to remix the different parts of a signal: use the left knob to define the balance between the sinusoidal and noisy contributions. Use the right knob to deal with the balance between Attacks and the rest of the audio signal.

![Remix section](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/remix_section.png)

The Settings tab gives an access to the configuration of the SuperVP engine:

![Settings tab](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/settings_tab.png)

**FFT Size:** a value of 4096 samples works fine for most polyphonic sounds. If you work on rhythmic material or if you want to reduce the latency, you should change this value to 2048 or 1024.
**Preserve Waveform:** this function should be on only when working on a single speaking or singing voice.
**Preserve Attacks:** this function switches on the transient preservation of the SuperVP algorithm. This option should be on when working on rhythmical material.
**Delay compensation:** We know that latency is crucial in a live situation. Therefore we tried to implement the most flexible solution to deal with it in our devices. The latency of the devices is not fixed; changing the settings of the plugin can optimize it. The actual latency is displayed bellow the settings. You know then exactly the amount of latency added by the device.
The delay compensation can be switched off but the Dry/Wet signals will no longer be aligned.

>**WARNING:** after any parameter change in the Settings section, you have to turn off and on again the Delay Compensation in the Options menu of Live in order to make it understand that the latency of the device has changed.

- **IM-Transp**
In this device, we added two sequencers to modify both, the pitch transposition amount and the spectral envelope transposition amount.

![Frmts tab](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/transp_full.png)

Use the Frmts tab to access to the second step sequencer.

![formants tab](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/formants_tab.png)

The other sections of the device are the same as in IM-SimpleTransp.

-	**IM-Scrub**
IM-Scrub adds a position Matrix and scrub/scratch functions to the Transp device. It can also record audio on the fly and can reorder sound material divided in slices.
The leftmost section of the device allows to record audio from the current audio track on the fly. It is also possible to load a sample by dragging a sample on the waveform area. Start and Length parameters define which part of the sound is played by the device.

![record section](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/record_section.png)

The Slice matrix defines which slice to play for each step of the sequence: the x axis represents the time, the y axis represents a start position in the sound file.

![Slice matrix](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/capture-d%E2%80%99e%CC%81cran-2014-07-03-a%CC%80-12.57.35.png)

The other sections of the device are the same as in IM-Transp.

-	**IM-Mover**
IM-Mover proposes  a different way to control time position and transposition performed by the SuperVP engine. It displays the sound on a big waveform on which the user can move using the mouse, its MIDI controllers or a joystick. When Mover is activated, it mixes or replaces the audio that runs on the same track depending of the selected mode on the rightmost part of the device.

![IM-Mover](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/mover_full.png)

The leftmost section of the device has the same functions as in IM-Scrub. We added here the parameters of an ADSR envelope that multiplies the output of Mover for fades in and fades out.

![leftmost section](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/record_env_section.png)

Drag the mouse on the central waveform to read and transpose the current sound at any position.

![Drag](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/waveform.png)

Use the joystick tab to assign the main commands of the device to a joystick. When you select a new external device, a calibration system is used in order to adapt  the mapping depending of the datas generated by the joystick. Move the joystick in all its direction with the maximum amplitude to calibrate it. The new values for the calibration are memorized with the Live session. Reset the calibration by pressing « R ».

![JoyStick section](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/joystick_section.png)

The current mode of Mover is defined on the rightmost section of the device: in Mix mode, the signal produced by the Mover is mixed with the audio playing on the same track. In Replace mode, it replace the signal of the audio track.

![Mix Replace](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/mix_replace.png)

-	**IM-SuperVPSynth**
IM-SuperVPSynth embeds the SuperVP technology into a sample player instrument with more or less the same functions as in the Live’s Simpler device.

![IM-SuperVPSynth](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/supervpsynth_full.png)

The leftmost part of the device is dedicated to the sample selection, playback and pitch parameters. Set the Ref pitch (i. e. the real pitch of the sample) correctly to ear the correct pitches when playing from a MIDI keyboard. To play a smaller selection of a sound file, choose the part that you want to play by doing a selection with the mouse on the waveform.
To achieve a selection with more precisions, you can activate the Zoom mode. To playback the sample, we use an LFO that can be free or synchronized on the Live tempo. When Sync is off, use the speed knob to slow down or accelerate the audio (without changing its pitch). Pitch and formant transpositions are also available.

![playback section](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/playback_section.png)

The next section provides a multimode filter and the parameters of the Amplitude envelope.

![Filtre section](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/filter_section.png)

The next section offers an LFO and a Modulation envelope that you can send on various destinations as the Filter Frequency or the SinusNoise parameters of the Remix options…

![LFO Modulation section](https://forum.ircam.fr/media/uploads/Softwares/IRCAM%20Collection%201/modulation_section.png)

The Remix and Settings tab are very similar to those of the other SuperVP devices. We just allow to modulate some parameters with the velocity as the SinusNoise and Attacks parameters of the remix options.

> - **Contents**: 9 Max Devices, 3 Live Clips, 34 Presets, 1 Live Sets 
> - **Size**: Installation size: 379.27 MB ; Download size: 370.5 MB 
> - **Requirements**: Live 9 Standard (version 9.1.7 or higher) ; Max for Live 
> - **Note: This pack requires. Live version 9.0.1 or higher and Max 6.1.8 or higher**

> - [Vidéos](https://www.youtube.com/playlist?list=PL6MqWe5aRuOAYOiUQnBMnbE2twayrZgAv)



